# import graphene
# import graphql_jwt
# from graphene_django import DjangoObjectType
# from django.contrib.auth.models import AbstractUser
# from django.contrib.auth import get_user_model
# from graphql_jwt.decorators import login_required

# class AppUserType(DjangoObjectType):
#     class Meta:
#         model = get_user_model()
#         exclude = ('password', )

# # class CustomUser(DjangoObjectType):
# #     class Meta:
# #         model = get_user_model()
# #         exclude = ('password', )
# #         model.nickname = graphene.String()

# class Query(graphene.ObjectType):
#     app_users=graphene.List(AppUserType)
#     logget_in = graphene.Field(AppUserType)

#     def resolve_app_users(self, info):
#         return get_user_model().objects.all()

#     @login_required
#     def resolve_logged_in(self, info):
#         return info.context.user

# class CreateAppUser(graphene.Mutation):
#     app_user = graphene.Field(AppUserType)

#     class Arguments:
#         email = graphene.String()
#         username = graphene.String()
#         lastname = graphene.String()
#         password = graphene.String()

#     def mutate(self, info, email, username, lastname, password):
#         app_user = get_user_model()
#         new_user = app_user(email=email,username=username, lastname=lastname)
#         new_user.set_password(password)
#         new_user.save()
#         return CreateAppUser(app_user=new_user)




# class Mutation(graphene.ObjectType):
#     create_app_user = CreateAppUser.Field()
#     token_auth = graphql_jwt.ObtainJSONWebToken.Field()
#     verify_token = graphql_jwt.Verify.Field()
#     refresh_token = graphql_jwt.Refresh.Field()


# schema = graphene.Schema(query=Query, mutation=Mutation)
# ____________________________

import graphene
import graphql_jwt
from graphene_django import DjangoObjectType
from django.contrib.auth import get_user_model
from graphql_jwt.decorators import login_required

class AppUserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        exclude = ('password', )

class Query(graphene.ObjectType):
    app_users=graphene.List(AppUserType)
    logget_in = graphene.Field(AppUserType)

    def resolve_app_users(self, info):
        return get_user_model().objects.all()

    @login_required
    def resolve_logged_in(self, info):
        return info.context.user

class CreateAppUser(graphene.Mutation):
    app_user = graphene.Field(AppUserType)

    class Arguments:
        email = graphene.String()
        username = graphene.String()
        country = graphene.String()
        firstName = graphene.String()
        lastName = graphene.String()
        nickName = graphene.String()
        password = graphene.String()
        UUID = graphene.BigInt()

    def mutate(self, info, email, username, password, nickName='', firstName='', lastName='', country='', UUID=0):
        app_user = get_user_model()
        if(nickName==''):
            nickName=username
        
        new_user = app_user(email=email,username=username, country=country, first_name=firstName, last_name=lastName, UUID=UUID, nickName=nickName)
        new_user.set_password(password)
        new_user.save()
        return CreateAppUser(app_user=new_user)




class Mutation(graphene.ObjectType):
    create_app_user = CreateAppUser.Field()
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)

# _______________________________


# import graphene
# from graphql_auth import mutations
# from graphql_auth.schema import UserQuery, MeQuery

# class AuthMutation(graphene.ObjectType):
#    register = mutations.Register.Field()
#    verify_account = mutations.VerifyAccount.Field()
#    token_auth = mutations.ObtainJSONWebToken.Field()
#    update_account = mutations.UpdateAccount.Field()
#    resend_activation_email = mutations.ResendActivationEmail.Field()
#    send_password_reset_email = mutations.SendPasswordResetEmail.Field()
#    password_reset = mutations.PasswordReset.Field()
#    password_change = mutations.PasswordChange.Field()

# class Query(UserQuery, MeQuery, graphene.ObjectType):
#     pass

# class Mutation(AuthMutation, graphene.ObjectType):
#    pass

# schema = graphene.Schema(query=Query, mutation=Mutation)