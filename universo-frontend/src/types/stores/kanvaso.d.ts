import ObjectWrapper from 'src/utils/objectWrapper';

export interface SimpleContent {
  enhavo: string;
}

export interface EdgeOwner {
  uuid: string;
  objId: number;
  // Дополнительные поля могут быть добавлены здесь
}

export interface Edge {
  node: Node;
}

export interface Posedanto extends EdgeOwner {
  edges: Edge[];
}

export interface CommonObjectFields {
  uuid: string;
  forigo: boolean;
  publikigo: boolean;
  nomo: SimpleContent;
  priskribo: SimpleContent;
  objId: number;
  // Дополнительные поля могут быть добавлены здесь
}

// Определение TreeNode

export type TreeNode = {
  pozicio: number | null;
  koordinatoX: number | null;
  koordinatoY: number | null;
  largxo: number | null;
  longo: number | null;
  setChildrens(node: ObjectWrapper | ObjectWrapper[]): void;
  name: string;
  isPublished: boolean;
  isDeleted: boolean;
  description: string;
  type: string;
  id: number;
  typeId: number;
  uuid: string;
  parentUuid: string;
  childrens: ObjectWrapper[];
  hasParent: boolean;
  forigo: boolean;
  publikigo: boolean;
  nomo: Nomo;
  priskribo: Priskribo;
  objId: number;
  tipo: Tipo;
  posedanto: Posedanto;
  kanvasojKanvasoobjektoligiloLigilo: KanvasojKanvasoobjektoligiloLigilo;
};

// Другие определения

export interface KanvasojKanvasoObjekto {
  edges: any[];
}
export interface KanvasojKanvasoobjektoligiloLigilo {
  posedanto: Posedanto;
  tipo: Tipo;
}
export interface RootObject {
  kanvasojKanvasoObjekto: KanvasojKanvasoObjekto;
  edges: Edge[];
  kanvasojKanvaso: KanvasojKanvaso;
  kanvasoEventoj: KanvasoEventoj;
}

export interface KanvasojKanvaso {
  edges: Edge[];
}

export interface Kategorio {
  edges: any[];
}

export interface Tipo {
  uuid: string;
  objId: number;
  nomo: SimpleContent;
  priskribo: SimpleContent;
}

export interface Node extends CommonObjectFields {
  pozicio?: any;
  kanvasojKanvasoobjektoligiloLigilo?: KanvasojKanvasoobjektoligiloLigilo;
  posedantoUzanto: PosedantoUzanto;
  kategorio: Kategorio;
  kanvasoObjekto: KanvasoObjekto;
  posedanto: Posedanto;
}

export interface KanvasoObjekto {
  edges: Edge[];
}

export interface PosedantoUzanto extends EdgeOwner {
  // Дополнительные поля специфичные для PosedantoUzanto
}

export interface Kanvaso extends CommonObjectFields {
  tipo?: Tipo;
  pozicio?: any;
  posedanto: Posedanto;
  // Дополнительные поля специфичные для Kanvaso
}

export interface Objekto extends CommonObjectFields {
  kanvasojKanvasoobjektoligiloLigilo?: KanvasojKanvasoobjektoligiloLigilo;
  tipo: Tipo;
  pozicio?: any;
  kanvaso: Kanvaso;
}

export interface KanvasoEventoj {
  evento: string;
  objekto: Objekto;
  objektoLigilo?: any;
}
