export function LoadingResources(scene) {
  scene.load.image('icon', 'phaser/stretching_icon.png');
  scene.load.image('logo', 'phaser/logo_202x200.png');
  scene.load.image('universo', 'phaser/universo_96x96.png');

  scene.load.image('toolbar_1a', 'phaser/toolbarButtons/UniversoIcons1_1a.png');
  scene.load.image('toolbar_1c', 'phaser/toolbarButtons/UniversoIcons1_1c.png');
  scene.load.image('toolbar_1b', 'phaser/toolbarButtons/UniversoIcons1_1b.png');

  scene.load.image('toolbar_2a', 'phaser/toolbarButtons/UniversoIcons1_2a.png');
  scene.load.image('toolbar_2c', 'phaser/toolbarButtons/UniversoIcons1_2c.png');
  scene.load.image('toolbar_2b', 'phaser/toolbarButtons/UniversoIcons1_2b.png');

  scene.load.image('toolbar_3a', 'phaser/toolbarButtons/UniversoIcons1_3a.png');
  scene.load.image('toolbar_3c', 'phaser/toolbarButtons/UniversoIcons1_3c.png');
  scene.load.image('toolbar_3b', 'phaser/toolbarButtons/UniversoIcons1_3b.png');

  scene.load.image('toolbar_4a', 'phaser/toolbarButtons/UniversoIcons1_4a.png');
  scene.load.image('toolbar_4c', 'phaser/toolbarButtons/UniversoIcons1_4c.png');
  scene.load.image('toolbar_4b', 'phaser/toolbarButtons/UniversoIcons1_4b.png');

  scene.load.image('editicon', 'phaser/editicon_880x881.png');

  scene.load.image('plus', 'sprite/plus_150x150.png');
  scene.load.image('minus', 'sprite/minus_150x150.png');
  scene.load.image('iconZ', 'sprite/blockWithCroppedCorners_109x37.png');
  scene.load.image('fullscreen', 'sprite/fullscreen.png');
}
