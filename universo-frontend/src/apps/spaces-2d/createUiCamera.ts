export default (scene) => {
  // Создаем отдельную камеру для UI, которая не будет двигаться
  scene.cameras.add(
    0,
    0,
    scene.game.scale.width,
    scene.game.scale.height,
    false,
    'ui',
  );
  scene.cameras.getCamera('ui').setScroll(0, 0); // Фиксируем положение камеры для UI
  scene.cameras.getCamera('ui').ignore(scene.children.list); // Игнорируем все объекты сцены для этой камеры
};
