import { COLOR_BACKGROUND } from '../kanbans/Const';
import { EndlessCanvas } from './EndlessCanvas';

const CreateDOM = (scene: EndlessCanvas, content: string) => {
  //@ts-ignore
  const dom = scene.add.dom().createFromHTML(content);
  //@ts-ignore
  dom.node.style.width = '400px';
  dom.updateSize();
  return dom;
};
export default (scene) => {
  const content =
    'Phaser is a fast, free, and fun open source HTML5 game framework that offers WebGL and Canvas rendering across desktop and mobile web browsers. Games can be compiled to iOS, Android and native apps by using 3rd party tools. You can use JavaScript or TypeScript for development.';

  const el = document.createElement('div');
  el.innerHTML = content;
  el.style.width = '400px';
  el.style.backgroundColor = `#${COLOR_BACKGROUND.toString(16)}`;

  // const domElement = CreateDOM(scene, content);
  const domElement = scene.add.dom(0, 0, el);
  domElement.updateSize();
  domElement.setPosition(500, 500);
  // domElement.setDepth(1000);
  domElement.setVisible(true);
  scene.children.bringToTop(domElement);
  //https://rexrainbow.github.io/phaser3-rex-notes/docs/site/gameobject/#will-render
  console.log(domElement.willRender(scene.cameras.main));
  console.log(domElement.willRender(scene.cameras.getCamera('ui')));
  const willRender =
    domElement.renderFlags === Phaser.GameObjects.GameObject.RENDER_MASK;
  console.log(willRender);

  return domElement;
};
