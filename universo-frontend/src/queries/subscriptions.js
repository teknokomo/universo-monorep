import gql from 'graphql-tag';

export const docsSubscriptions = gql`
  subscription docsSubscriptions($organizojUuid: [String]!) {
    dokumentoEkspedoEventoj(organizojUuid: $organizojUuid) {
      evento
      objekto {
        __typename
      }
      dokumento {
        uuid
        ekspedo {
          edges {
            node {
              uuid
            }
          }
        }
      }
    }
  }
`;

export const KanvasoEventoj = gql`
  subscription KanvasoEventoj($kanvasoj: [Int]!) {
    KanvasoEventoj(kanvasoj: $kanvasoj) {
      evento
      objekto {
        uuid
        forigo
        publikigo
        koordinatoX
        koordinatoY
        largxo
        longo
        kanvasojKanvasoobjektoligiloLigilo {
          uuid
          forigo
          publikigo
          posedanto {
            uuid
            objId
          }
          tipo {
            objId
            nomo {
              enhavo
            }
          }
        }
        tipo {
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
        }
        pozicio
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        objId
        kanvaso {
          uuid
          forigo
          publikigo
          tipo {
            objId
            nomo {
              enhavo
            }
            priskribo {
              enhavo
            }
          }
          pozicio
          posedanto {
            edges {
              node {
                uuid
                forigo
                publikigo
                posedantoUzanto {
                  uuid
                  objId
                }
                tipo {
                  objId
                  nomo {
                    enhavo
                  }
                  priskribo {
                    enhavo
                  }
                }
              }
            }
          }
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          objId
        }
      }
      objektoLigilo {
        ligilo {
          objId
          uuid
          kanvaso {
            objId
            uuid
          }
        }
        posedanto {
          objId
          uuid
          kanvaso {
            objId
            uuid
          }
        }
      }
    }
  }
`;
