extends Node


# запрос на список языков
func get_lingvo(id):
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "query  "+
		"{ informilojLingvoj { edges { node { " +
		" uuid nomo flago"+
		" kodo " +
		" } } } } ",
	} })
	# if Global.logs:
	# 	print("=== get_lingvo = ",query)
	return query


# запрос на получение реальности
func get_realeco(id):
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "query  "+
		"{ realeco{ edges { node { " +
		" uuid nomo { enhavo json } priskribo { enhavo json } "+
		" objId forigo publikigo arkivo " +
		" kodo " +
		" } } } } ",
	} })
	# if Global.logs:
	# 	print("=== get_realeco = ",query)
	return query

# запрос на получение типа приложения 
func get_apliko_tipo(id): #не готово
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "query  "+
		"{ universoAplikoTipo{ edges { node { " +
		" uuid nomo { enhavo json } priskribo { enhavo json } "+
		" objId forigo publikigo arkivo " +
		" } } } } ",
	} })
	# if Global.logs:
	# 	print("=== get_realeco = ",query)
	return query


# изменение реальности
func shanghi_realeco(uuid, kodo, nomoJson, priskriboJson):
	var id = Net.get_current_query_id()
	Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation  ($uuid:UUID, $nomoJson:GenericScalar, '+
		" $priskriboJson:GenericScalar, $kodo:String) "+
		" { redaktuRealeco (uuid:$uuid, nomoJson:$nomoJson, "+
		" priskriboJson:$priskriboJson, kodo:$kodo) "+
		' {status message realeco{ uuid } } }',
		'variables': {"uuid": uuid, "nomoJson":nomoJson, 
		"priskriboJson":priskriboJson, "kodo":kodo } }})
	# if Global.logs:
	# 	print('===shanghi_realeco=',query)
	return query
	

# добавление реальности
func aldoni_realeco(kodo, nomoJson, priskriboJson):
	var id = Net.get_current_query_id()
	Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation  ($nomoJson:GenericScalar,'+
		" $priskriboJson:GenericScalar, $kodo:String) "+
		" { redaktuRealeco (nomoJson:$nomoJson, "+
		" priskriboJson:$priskriboJson, kodo:$kodo, "+
		" publikigo:true) "+
		' {status message realeco{ uuid } } }',
		'variables': {"nomoJson":nomoJson, 
		"priskriboJson":priskriboJson, "kodo":kodo } }})
	# if Global.logs:
	# 	print('===aldoni_realeco=',query)
	return query
	
# удаление реальности
func forigi_realeco(uuid, id): 
	Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation  ($uuid:UUID) '+
		' { redaktuRealeco (uuid:$uuid, forigo:true) '+
		' {status message realeco{ uuid } } }',
		'variables': {"uuid": uuid} }})
	# if Global.logs:
	# 	print('===shanghi_realeco=',query)
	return query	

# добавление справочника # не работает (11.06.21)
func aldoni_applico_tipo(kodo, nomoJson, priskriboJson):
	var id = Net.get_current_query_id()
	Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation  ($nomoJson:GenericScalar,'+
		" $priskriboJson:GenericScalar) "+
		" { redaktuUniversoAplikoTipo (nomoJson:$nomoJson, "+
		" priskriboJson:$priskriboJson "+
		" publikigo:true) "+
		' {status message universoAplikoTipo{ uuid } } }',
		'variables': {"nomoJson":nomoJson, 
		"priskriboJson":priskriboJson} }})
	if Global.logs:
		print('===aldoni_apliko_tipo===',query)
	return query

# изменение справочника  # не доделано (11.06.21)
func shanghi_applico_tipo(uuid, kodo, nomoJson, priskriboJson):
	var id = Net.get_current_query_id()
	Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation  ($uuid:UUID, $nomoJson:GenericScalar, '+
		" $priskriboJson:GenericScalar) "+
		" { redaktuUniversoAplikoTipo (uuid:$uuid, nomoJson:$nomoJson, "+
		" priskriboJson:$priskriboJson) "+
		' {status message universoAplikoTipo{ uuid } } }', # нужно ли менять на AplikoTipo
		'variables': {"uuid": uuid, "nomoJson":nomoJson, 
		"priskriboJson":priskriboJson} }})
	# if Global.logs:
	# 	print('===shanghi_realeco=',query)
	return query

# удаление справочника  
func forigi_applico_tipo(uuid, id):
	Net.net_id_clear.push_back(id)
	var query = JSON.stringify({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation  ($uuid:UUID) '+
		' { redaktuUniversoAplikoTipo (uuid:$uuid, forigo:true) '+
		' {status message universoAplikoTipo{ uuid } } }',
		'variables': {"uuid": uuid} }})
	if Global.logs:
		print('\n===forigi_applico_tipo===',query)
	return query	
