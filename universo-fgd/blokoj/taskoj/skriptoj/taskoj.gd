extends "res://kerno/fenestroj/tipo_a1.gd"

const QueryObject = preload("queries.gd")

var id_projekto = []
var id_taskoj = []

var listoProjektoj = [] # список проектов
var projektoUuid = '' # если загружеены задачи по проекту, то установлен какой именно проект
var listoTaskoj = [] #список задач по проекту

func _ready():
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", Callable(self, "_on_data"))
	if err:
		print('Ошибка подключения связи с обменом данных = ',err)


# добавить новые проекты в список проектов
func aldoni_projektoj(projektoj):
	listoProjektoj.append_array(projektoj)
	FillItemList()


# добавить новые проекты в список задач
func aldoni_taskoj(taskoj):
	listoTaskoj.append_array(taskoj)
	FillItemList()


func clear_list():
	$VBox/body_texture/ItemList.clear()


func clear_all():
	listoProjektoj.clear()
	clear_taskoj()


func clear_taskoj():
	projektoUuid = ''
	$VBox/HBox_top/top_texture2/HBox_buttons/menuo_name.text = "Проекты"
	listoTaskoj.clear()


func _on_data():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		var index_projekto = id_projekto.find(int(on_data['id']))
		var index_taskoj = id_taskoj.find(int(on_data['id']))
		if index_projekto > -1: # находится в списке отправленных запросов
			aldoni_projektoj(on_data['payload']['data']['taskojProjekto']['edges'])
			if on_data['payload']['data']['taskojProjekto']['pageInfo']['hasNextPage']:
				mendo_informoj_projekto(on_data['payload']['data']['taskojProjekto']['pageInfo']['endCursor'])
			# удаляем из списка
			id_projekto.remove(index_projekto)
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		elif index_taskoj > -1: # находится в списке удаляемых объектов
			aldoni_taskoj(on_data['payload']['data']['taskojTasko']['edges'])
			if on_data['payload']['data']['taskojTasko']['pageInfo']['hasNextPage']:
				mendo_informoj_tasko(on_data['payload']['data']['taskojTasko']['pageInfo']['endCursor'])
			# удаляем из списка
			id_taskoj.remove(index_taskoj)
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)

# перезагружаем список объектов
func _reload_taskoj():
	$"VBox/body_texture/ItemList".clear()
	FillItemList()


func FillItemList():
	clear_list()
	# Заполняет список найденными продуктами
	if projektoUuid:
		# если задачи смотрим
		get_node("VBox/body_texture/ItemList").add_item('..')
		for Item in listoTaskoj:
			get_node("VBox/body_texture/ItemList").add_item(Item['node']['nomo']['enhavo'], null, true)
	else:
		# выводим список проектов
		for Item in listoProjektoj:
			get_node("VBox/body_texture/ItemList").add_item(Item['node']['nomo']['enhavo'], null, true)


# Загружаем данные задач
func mendo_informoj_tasko(after=""): # заказ сведений
	if !after:
		clear_list()
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	id_taskoj.push_back(id)
	# Делаем запрос к бэкэнду
	Net.send_json(q.taskoj_query(projektoUuid, id, after))


# Загружаем данные проектов
func mendo_informoj_projekto(after=""): # заказ сведений
	if !after:
		clear_list()
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	id_projekto.push_back(id)
	# Делаем запрос к бэкэнду
	Net.send_json(q.projekto_query(id, after))




func _on_ItemList_item_activated(index):
	if (projektoUuid == ""):
		# заходим в проект
		clear_taskoj()
		$VBox/HBox_top/top_texture2/HBox_buttons/menuo_name.text = "Задачи"
		projektoUuid = listoProjektoj[index]['node']['uuid']
		mendo_informoj_tasko()
	elif index == 0:
		# возвращаемся в список проектов
		clear_taskoj()
		FillItemList()


func _on_ItemList_focus_entered():
	fenestro_supren()

