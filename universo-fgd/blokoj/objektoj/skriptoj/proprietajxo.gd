extends "res://kerno/fenestroj/tipo_d1.gd"
# proprietaĵo - имущество (собственность) proprietajxo

const QueryObject = preload("queries.gd")


@onready var table = get_node("VBox/body_texture/HSplit/VBoxContainer2/Table")
@onready var bottom = get_node("VBox/body_texture/HSplit/VBoxContainer2/HBoxContainer2")


var id_mendi_informoj #запросить данные
var konservejo_aktuala # текущая выделенная строка

# Массив объектов в собственности
var proprietajxoj = []


func _ready():
	var err = Net.connect("input_data", Callable(self, "_on_data"))
	if err:
		print('error = ',err)
	offset_left = 150
	offset_top = 150


func _on_data():
	var i_data_server = 0
	var masivo_forigo = [] # массив индексов на удаление
	for on_data in Net.data_server:
		if int(on_data['id']) == id_mendi_informoj:
			# получили данные с сервера
			plenigi_formularon(on_data['payload']['data'])
			masivo_forigo.insert(0, i_data_server) # удаляем после окончания цикла
			id_mendi_informoj = 0
		i_data_server += 1
	for forigo in masivo_forigo:
		Net.data_server.remove(forigo)


# запросить данные с сервера
# заказать - mendi
# сведения - informoj
func mendi_informoj():
	var query = QueryObject.new()
	id_mendi_informoj = Net.get_current_query_id()
	Net.send_json(query.get_proprietajxo_objekto(id_mendi_informoj))


#plenigi formularon - заполнить форму
#	Своё имущество, где бы оно не находилось
func plenigi_formularon(on_data):
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	tree.clear()
	clear_listo()
	tree.set_hide_root(true)
	var test = tree.create_item(null,-1)
	test.set_text(0,'скрытый')
	# склады в разных модулях
	for objekto in on_data['filteredObjektoPosedanto']['edges']:
		var objekto_node = tree.create_item()
		objekto_node.set_text(0, objekto['node']['nomo']['enhavo'])
		objekto_node.set_metadata(0,objekto.duplicate(true)) 


# очищаем окно 
func clear_listo():
	table.clear()


# plenigi - заполнить
func plenigi_listo(listoj):
	if not listoj.get('stokejo'):# нет склада
		return
#	var items = listoj['stokejo']['objekto']['edges']
#	var modulo = listoj['modulo']
#	clear_listo()
#	var i = 1
#	var volumeno = 0
#	table.columns = 4
#	table.set_hide_root(true)
#	# заполняем заголовки столбцов
#	table.set_column_title(0,'')
#	table.set_column_title(1,'внутренний объём')
#	table.set_column_title(2,'внешний объём')
#	table.set_column_title(3,'объём хранения')
#	table.set_column_titles_visible(true)
#	table.create_item()
#	var item
#
#	# enteno - содержа́ние (одной субстанции в другой)
#	for enteno in items:
#		item = table.create_item()
#		# сохраняес данные строки, что бы точно идентифицировать
#		item.set_metadata(0,enteno.duplicate(true))
#		item.set_text(0,str(i) + ')' +
#			String(enteno['node']['nomo']['enhavo']))
#		if enteno['node']['volumenoInterna']:
#			item.set_text(1,String(enteno['node']['volumenoInterna']))
#		else:
#			item.set_text(1,'-')
#		if enteno['node']['volumenoEkstera']:
#			item.set_text(2,String(enteno['node']['volumenoEkstera']))
#		else:
#			item.set_text(2,'-')
#		if enteno['node']['volumenoStokado']:
#			item.set_text(3,String(enteno['node']['volumenoStokado']))
#		else:
#			item.set_text(3,'-')
#		if enteno['node']['volumenoStokado']:
#			volumeno += enteno['node']['volumenoStokado']
#		i += 1
#	if modulo.get('volumenoInterna') and modulo['volumenoInterna']:
#		bottom.get_node('all').text = String(modulo['volumenoInterna'])
#	else:
#		bottom.get_node('all').text = '-'
#	bottom.get_node('free').text = String(volumeno)


# выбор склада
func _on_Tree_cell_selected():
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	var select = tree.get_next_selected(null)
	var konservejo = select.get_metadata(0)
	clear_listo()
	if !konservejo:
		return null
	plenigi_listo(konservejo)
	# enteno - содержа́ние (одной субстанции в другой)


func _on_toStacio_pressed():
	pass


func _on_Tree_focus_entered():
	fenestro_supren()


func _on_Table_focus_entered():
	fenestro_supren()


func _on_HBoxContainer2_focus_entered():
	fenestro_supren()


func _on_VBox_visibility_changed():
	# print('===_on_VBox_visibility_changed === ',$VBox.visible)
	pass # Replace with function body.
